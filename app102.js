function a() {
    console.log("Callback Function")
}
//Nested function
function saiHi(callback,fname,lname) {
    callback();
    function getFullname(){
        return fname + " " + lname;
    }
    return getFullname()
}
const message = sayHi("Mr.Mark","Zuckerberg")
console.log(message)